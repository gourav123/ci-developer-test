<?php
 class Mcommon extends CI_Model {
    function __construct(){
        parent::__construct();
    }
    public function insert($table,$data){
        $this->db->insert($table,$data);     
        return $this->db->insert_id();
    }
    public function batch_insert($table,$data){
        $this->db->insert_batch($table,$data); 
        return 1; 
    }
    public function getDetails($table,$condition){
        $this->db->where($condition);
        $query=$this->db->get($table);
        //return $this->db->last_query();
        return $query->result_array(); 
    }
    public function getFullDetails($table){
        $query=$this->db->get($table);
        return $query->result_array();
    }
    public function getRow($table,$condition){
        $this->db->where($condition);        
        $query=$this->db->get($table);          
        $res = array();
        if($query->num_rows()>0){ 
            $res = $query->row_array();    
        }
        return $res;            
    }  

    public function getRowV2($table,$condition,$order_by=''){


        $this->db->where($condition);        
       
        if (isset($order_by)) {
          $this->db->order_by($order_by );          
          $this->db->limit(1 );          
        }
        $query=$this->db->get($table);  
        $res = array();
        if($query->num_rows()>0){ 
            $res = $query->row_array();    
        }
        return $res;            
    }  

    public function getRows($table, $condition, $order_by='', $group_by='',$limit = ''){
        $this->db->where($condition);        
        /*if($table=='tbl_package'){
            $this->db->order_by('category_id','asc');          
        }else{
            $this->db->order_by('date_of_creation', 'desc');
        }*/
        if (isset($order_by)) {
          $this->db->order_by($order_by);          
        }
         if (isset($limit)) {
          $this->db->limit($limit);          
        }

        if (isset($group_by)) {
          $this->db->group_by($group_by);          
        }
        
        $query=$this->db->get($table);          
        $res = array();
        if($query->num_rows()>0){ 
            $res = $query->result_array();    
        }

        return $res;            
    }  

    public function checkUser($table,$condition){
        $this->db->where($condition);
        $query=$this->db->get('admins');
        return $query->row_array(); 
    } 
    public function update($table,$condition,$data){
        $this->db->where($condition);
        $this->db->update($table,$data);
        //echo $this->db->last_query();
        return 1;
    }
    public function delete($table,$condition){ 
        $this->db->where($condition);
        $this->db->delete($table);
        return 1;
    }

    public function getsql($sql)
    {
        $query = $this->db->query($sql); 
        return $query->result_array();
    }
     public function get($table, $what = null, $condition = null, $limit_start = null, $limit_end = null, $group = null, $condition1 = null, $order_in = null, $order_by = 'DESC', $join = null, $join_type = null,$response = null)
    {
        if (isset($what)) {
            foreach ($what as $key => $value) {
                $this->db->select($value);
            }
        } else {
            $this->db->select('*');
        }

        $this->db->from($table);
        if (isset($condition)) {
            foreach ($condition as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        if (!empty($condition1)) {
            $this->db->where($condition1);
        }

        
        if (isset($join)) {
            foreach ($join as $key => $value) {
               
                $this->db->join($key, $value, $join_type);
                // $this->db->join($key, $value, $join_type[$key]);
            }
        }

        if ($limit_start != null) {
            $this->db->limit($limit_start, $limit_end);
        }
        if ($group != null) {
            $this->db->group_by($group);
        }

        if ($order_in != null) {
            $this->db->order_by($order_in, $order_by);
        }
        $query = $this->db->get();
        // echo $this->db->last_query();die;
        if ($response == 'result') {

            return   $query->result();
        } 
        elseif($response == 'row') {
            return $query->row();
        }
        elseif($response == 'row_array') {
            return $query->row_array();
        }
        else{
            return $query->result_array();
        }
        
    }
   


}
