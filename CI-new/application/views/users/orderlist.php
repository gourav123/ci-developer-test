
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Test Dashbaord</title>

  <!-- Bootstrap core CSS -->
  <link href="<?= base_url('assets/') ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?= base_url('assets/') ?>css/simple-sidebar.css" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body>

  <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
      <!-- <div class="sidebar-heading">Start Bootstrap </div> -->
      <div class="list-group list-group-flush">
        <a href="#" class="list-group-item list-group-item-action bg-light">Dashboard</a>
        <div class="list-group list-group-flush">
	        <a href="<?=base_url('users/orders'); ?>" class="list-group-item list-group-item-action bg-light">Orders</a>
	        
	     </div>
      </div>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

      <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
        <button class="btn btn-primary" id="menu-toggle">Toggle Menu</button>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item active">
              <a class="nav-link" href="#">Dashboard <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url('users/Logout') ?>">Logout</a>
            </li>
            
          </ul>
        </div>
      </nav>

      <div class="container-fluid">
        <h1 class="mt-4">Orders</h1>
        	<table id="example" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>OrderId</th>
                    <th>Oder Date</th>
                    <th>Shipped Date</th>
                    <th>Status</th>
                    <th>View Details</th>
                </tr>
            </thead>
            <tbody>
              <?php if(!empty($orders)){

                      foreach($orders As $order) {
              ?>
                <tr>
                    <td><?= $order['orderNumber']; ?></td>
                    <td><?= $order['orderDate']; ?></td>
                    <td><?= $order['shippedDate']; ?></td>
                    <td><?= $order['status']; ?></td>
                    <td><a href="<?= base_url('/orders/'). $order['orderNumber']; ?>">View Details</a></td>
                </tr>
              <?php } } ?>   
            </tbody>    
      </div>
    </div>
    <!-- /#page-content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Bootstrap core JavaScript -->
  <script src="<?= base_url('assets/') ?>vendor/jquery/jquery.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?= base_url('assets/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Menu Toggle Script -->
  <script>
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    $(document).ready(function() {
    $('#example').DataTable();
} );  
  </script>

</body>

</html>
