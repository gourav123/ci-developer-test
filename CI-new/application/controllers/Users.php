<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('Mcommon');
		 $this->load->helper(array('text','url'));
	    /*if($this->is_logged_in_user()){
	    	redirect(base_url('profile'));
	    }*/	    
 
	}

	public function index()
	{
		if($this->session->userdata('users') !=''){
	    	redirect(base_url('users/Dashboard'));
	    }
		 // $this->load->helper('text','url');
		$this->load->view('users/login');
	}

	public function login()
	{
		$postData =  $this->input->post();
		$details =  $this->Mcommon->getRow('user_master',array('email'=>$postData['phone'] ,'password'=>md5($postData['password'])));
		// print_r($details);die;
		if (!empty($details)) {
			$this->session->set_userdata('users',$details);
			redirect('users/dashboard');
		}else{
			redirect('users');
		}
		
	}
	public function Dashboard()
	{
		// print_r($this->session->userdata('users'));
		if($this->session->userdata('users') ==''){
	    	redirect(base_url('users'));
	    }
	    	$session_data =  $this->session->userdata('users')  ;
	    	if($session_data['role_id'] != '1')
				$data['message'] = 'Welcome Customer.'	;
			else
				$data['message'] = 'Welcome Admin.'	;
		$this->load->view('users/dashboard',$data);

	}
	public function orders()
	{
		$data['orders'] =  $this->Mcommon->getFullDetails('orders');
		// $data = array();
		$this->load->view('users/orderlist',$data);
		// print_r($orders);
	}

	public function orderdetails($id)
	{
		$details =  $this->Mcommon->getRow('orders',array('orderNumber '=>$id));
		
		if (empty($details)) {
			$response['status'] = '400';
			$response['message'] = 'No Order Found';
			$response['data'] = array();
		}else{
			// echo "<pre>";
		$condition =  array('order_details.orderNumber'=>$id);
        $join = array(
                            ' products' => ' products.productCode  = order_details.productCode ',
                        );
        $select = array('products.productName As product','products.productLine As product_line ','products.buyPrice AS unit_price','order_details.quantityOrdered As qty');
        $order_det =  $this->Mcommon->get( 'orderdetails As order_details',$select,$condition,"","","","","","DESC",$join,'INNER');
        $customer =  $this->Mcommon->getRow('customers',array('customerNumber'=>$details['customerNumber']));
        $i =0;  
        $sum = 0 ; 
        foreach ($order_det as  $value) {
        	$order_det[$i]['line_total'] =   $value['unit_price'] * $value['qty'];

        	$sum += $order_det[$i]['line_total']; 
        }
         $sum;
         $data = array(
         				'order_id' 	 	=> 	$details['orderNumber'],
         				'order_date' 	=> 	$details['orderDate'],
         				'status' 	 	=> 	$details['status'],
         				'order_details' => 	$order_det,
         				'bill_amount' 	=> 	$sum,
         				'customer'      => array( 'first_name' => $customer['contactLastName'],
         										  'last_name'  => $customer['contactFirstName'],
         										  'phone' 	   => $customer['phone'],
         										  'country_code'=> $customer['country'],
         										)
         			);
         	$response['status'] = '200';
			$response['message'] = 'No Order Found';
			$response['data'] = $data;
		}
		echo json_encode($response);

	}

	public function Logout()
	{
		$this->session->unset_userdata('users');
	    $this->session->set_flashdata('success_msg', "Logged out successfully.");
		redirect('users','refresh');
	}
}
